#ifndef racionalH
#define racionalH
#include <iostream>
using namespace std;

class Racional {
	public:
		Racional();
		Racional(int n, int d);
		int numerador() const;
		int denominador() const;
		void multiplicar(const Racional &r);
		void invertir();
		friend ostream& operator<<(ostream& os, const Racional& r);

	private:
		bool coprimos(int n, int d);
		int mcd(const int &n, const int &d);
		int num;
		int den;
};


#endif
