#include "racional.h"
#include <iostream>
using namespace std;

int Racional::mcd(const int &n, const int &d) {
	if(d==0) {
		return n;
	}
	if (n>d) {
		mcd(d,(n-d));
	} else {
		mcd(n, (d-n));
	}
}


bool Racional::coprimos(int n, int d) {
	return 1==mcd(n,d);
}

Racional::Racional() {
	num = 1;
	den = 1;
}

Racional::Racional(int n, int d) {
	if (coprimos(n,d)) {
		num = n;
		den = d;
	} else {
		int m = mcd(n,d);
		num = n / m;
		den = d / m;
	}
}

int Racional::denominador() const {
	return den;
}

int Racional::numerador() const {
	return num;
}

void Racional::multiplicar(const Racional &r) {
	num*=r.numerador();
	den*=r.denominador();
	int m = mcd(num, den);
	num = num / m;
	den = den / m;
}

void Racional::invertir() {
	int aux=this->num;
	this->num = this->den;
	this->den = aux;
}

ostream& operator<<(ostream& os, const Racional& r)
{
	os << r.numerador() << '/' << r.denominador();
	return os;
}



