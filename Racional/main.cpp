#include "racional.h"
#include <iostream>

using namespace std;


int main(int argn, char* args[]) {
    float num, den;
    cout << "Ingrese el valor para numerador: ";
    cin >> num;
    cout << "Ingrese el valor para denominador: ";
    cin >> den;
    Racional r1(num, den);

    cout << "Ingrese el valor para numerador: ";
    cin >> num;
    cout << "Ingrese el valor para denominador: ";
    cin >> den;
    Racional r2(num, den);

    cout << endl << "r1 =  " << r1 << endl;
    cout << endl << "r2 =  " << r2 << endl;


    r1.multiplicar(r2);
    cout << endl << "r1 =  " << r1 << endl;

    r2.invertir();
    cout << endl << "r2 =  " << r2 << endl;
    return 0;
}

