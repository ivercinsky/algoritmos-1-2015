#include "vector.h"
#include <cmath>
#include <iostream>
using namespace std;

Vector::Vector() {
	abscisa=0;
	ordenada=0;
}

Vector::Vector(float a, float b) {
	abscisa=a;
	ordenada=b;
}

int Vector::getAbscisa() const {
	return this->abscisa;
}

int Vector::getOrdenada() const {
	return this->ordenada;
}

bool Vector::igualX(const Vector v) {
	return this->abscisa == v.getAbscisa();
}

bool Vector::igualY(const Vector v) {
	return this->ordenada == v.getOrdenada();
}

float Vector::modulo() const{
	return sqrt (pow(this->abscisa, 2.0) + pow(this->ordenada, 2.0));
}

float Vector::prodEscalar(const Vector v) const {
	return (this->abscisa * v.getAbscisa()) + (this->ordenada * v.getOrdenada());
}

void Vector::resta(const Vector v) {
	this->abscisa -= v.getAbscisa();
	this->ordenada -= v.getOrdenada();
}

ostream& operator<<(ostream& os, const Vector& r)
{
	os << "(" << r.getAbscisa() << ',' << r.getOrdenada() << ")";
	return os;
}

istream& operator>>(istream& in, Vector& t)
{
           cout << "Ingresa el Vector." << endl;
           cout << "Abscisa: ";
           cin >> t.abscisa;
           cout << "Ordenada: ";
           cin >> t.ordenada;
           cout << "Ingresaste :" << t << endl;
           return in;
};

