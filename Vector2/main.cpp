#include "vector.h"
#include <iostream>
#include <cmath>

using namespace std;

float distancia(Vector v1, Vector v2){
	Vector r(v1.getAbscisa(), v1.getOrdenada());
	r.resta(v2);
	return sqrt ( pow (r.getAbscisa(), 2.0) + pow (r.getOrdenada(), 2.0));
}

int main(int argn, char* args[]) {
    float abscisa, ordenada;
    /*cout << "Ingrese el valor para abscisa: ";
    cin >> abscisa;
    cout << "Ingrese el valor para ordenada: ";
    cin >> ordenada;
    Vector v1(abscisa,ordenada);

    cout << "Ingrese el valor para abscisa: ";
    cin >> abscisa;
    cout << "Ingrese el valor para ordenada: ";
    cin >> ordenada;
    Vector v2(abscisa,ordenada);
	*/
    Vector v1;
    cin >> v1;
    cout << endl;
    Vector v2;
    cin >> v2;

    cout << v1 << endl;
    cout << v2 << endl;
    float dst = distancia(v1,v2);

    cout << endl << "La distancia es es " << dst << endl;
    return 0;
}

