#ifndef VectorH
#define VectorH
#include <iostream>
using namespace std;

class Vector {

    public:
        Vector();
        Vector(float a, float b);
        int getAbscisa() const;
        int getOrdenada() const;
        bool igualX(const Vector v);
        bool igualY(const Vector v);
        float modulo() const;
        float prodEscalar(const Vector v) const;
        void resta(const Vector v);

        friend ostream& operator<<(ostream& os, const Vector& r);

        friend istream& operator>>(istream& in, Vector& t);

    private:
        int abscisa;
        int ordenada;
};

#endif

