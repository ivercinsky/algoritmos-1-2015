/*
 * class Surtidor {

    public:
        Surtidor();
        Surtidor(int litros, int precio);
        int litrosDisponibles() const;
        int precioPorLitro() const;
        void expender(int cant);

    private:
        int ltsDisponibles;
        int precioXLitro;
};
 */

#include "surtidor.h"

Surtidor::Surtidor() {
	ltsDisponibles=1000;
	precioXLitro=10;
}

Surtidor::Surtidor(int litros, int precio) {
	ltsDisponibles=litros;
	precioXLitro=precio;
}

int Surtidor::litrosDisponibles() const {
	return ltsDisponibles;
}

int Surtidor::precioPorLitro() const {
	return precioXLitro;
}

void Surtidor::expender(int cant) {
	ltsDisponibles-=cant;
}
