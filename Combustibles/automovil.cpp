#include "automovil.h"
/*
 * class Automovil {

    public:
        Automovil();
        Automovil(int cap, int litros);
        int capacidadDelTanque() const;
        int litrosEnElTanque() const;
        void llenalo();

    private:
        int capTanque;
        int litrosEnTanque;
};
 *
 */

Automovil::Automovil(){
	capTanque=100;
	litrosEnTanque=100;
}

Automovil::Automovil(int cap, int litros){
	capTanque=cap;
	litrosEnTanque=litros;
}

int Automovil::capacidadDelTanque() const {
	return capTanque;
}

int Automovil::litrosEnElTanque() const {
	return litrosEnTanque;
}

void Automovil::llenalo() {
	litrosEnTanque=capTanque;
}
